import { GitlabtestPage } from './app.po';

describe('gitlabtest App', function() {
  let page: GitlabtestPage;

  beforeEach(() => {
    page = new GitlabtestPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
